<%-- 
    Document   : Archivo de peticiones
    Created on : dd/mm/yyyy, hh:mm: AM/PM
    Author     : nombre autor

--%>


<%@page import="java.lang.System"%>
<%@page import="modelo.Asignaturas"%>
<%@page import="modelo.Cursos"%>
<%@page import="modelo.Docentes"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="application/json;charset=iso-8859-1" language="java" pageEncoding="iso-8859-1" session="true"%>

<%    // Iniciando respuesta JSON.
    String respuesta = "{";

    //Lista de procesos o tareas a realizar 
    List<String> tareas = Arrays.asList(new String[]{
        "actualizarasignatura",
        "eliminarasignatura",
        "listarasignatura",
        "guardarasignatura",
        "listarcursos",
        "listardocentes"
    });
    
    String proceso = "" + request.getParameter("proceso");
    Asignaturas c = new Asignaturas();
    Cursos cu = new Cursos();
    Docentes doc = new Docentes();
    
    // Validaci�n de par�metros utilizados en todos los procesos.
    if (tareas.contains(proceso)) {
        respuesta += "\"ok\": true,";
        // ------------------------------------------------------------------------------------- //
        // -----------------------------------INICIO PROCESOS----------------------------------- //
        // ------------------------------------------------------------------------------------- //
        if (proceso.equals("guardarasignatura")) {
            
            
            String nombre = request.getParameter("nombre");  
            String docente = request.getParameter("docente");
            int curso = Integer.parseInt(request.getParameter("curso"));
  
            c.crearAsignatura(nombre,curso,docente);

            //System.out.println(c.getNombre());

            
            if (c.guardarAsignatura()) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }

        } else if (proceso.equals("eliminarasignatura")) {

            int id = Integer.parseInt(request.getParameter("id"));
             
             System.out.println("en eliminarasignatura");
             System.out.println(id);
             
            if (c.borrarAsignatura(id)) {

                
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }


          // Agregado listarcursos 

        } else if (proceso.equals("listarcursos")) {
            //su codigo ac�
            try {
                List<Cursos> lista = cu.getListaCursos();
                respuesta += "\"" + proceso + "\": true,\"Cursos\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Cursos\":[]";
                Logger.getLogger(Cursos.class.getName()).log(Level.SEVERE, null, ex);

            }
            
        //--------------------------------------------------
               }
        //------------------Listar Docente---------------------
        
        
        else if (proceso.equals("listardocentes")) {
            //su codigo ac�
            try {
                List<Docentes> lista = doc.getListaDocentes();
                respuesta += "\"" + proceso + "\": true,\"Docentes\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Docentes\":[]";
                Logger.getLogger(Docentes.class.getName()).log(Level.SEVERE, null, ex);

            }
            
        //--------------------------------------------------
               }else if (proceso.equals("listarasignatura")) {
            //su codigo ac�
            try {
                List<Asignaturas> lista = c.getListaAsignaturas();
                respuesta += "\"" + proceso + "\": true,\"Asignaturas\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Asignaturas\":[]";
                Logger.getLogger(Asignaturas.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("actualizarasignatura")) {
            
            int id = Integer.parseInt(request.getParameter("id").trim());
            String nombre = request.getParameter("nombre");
            String docente = request.getParameter("docente");
            int curso = Integer.parseInt(request.getParameter("curso"));
            
            c.crearAsignaturas(id,nombre,curso,docente);
            //su codigo ac�
            if (c.actualizarAsignatura()) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }
        
                    }
        // ------------------------------------------------------------------------------------- //
        // -----------------------------------FIN PROCESOS-------------------------------------- //
        // ------------------------------------------------------------------------------------- //
        // Proceso desconocido.
    } else {
        respuesta += "\"ok\": false,";
        respuesta += "\"error\": \"INVALID\",";
        respuesta += "\"errorMsg\": \"Lo sentimos, los datos que ha enviado,"
                + " son inv�lidos. Corrijalos y vuelva a intentar por favor.\"";
    }
    // Usuario sin sesi�n.
    // Responder como objeto JSON codificaci�n ISO 8859-1.
    respuesta += "}";
    response.setContentType("application/json;charset=iso-8859-1");
    out.print(respuesta);
%>
