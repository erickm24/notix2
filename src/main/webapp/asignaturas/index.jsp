<%-- 
    Document   : index
    Created on : 20/09/2021, 4:32:03 p. m.
    Author     : siste
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Asignaturas</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src = "http://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script> 
        <script src = "asignaturasController.js"></script>
        <title>Listado de Asignaturas</title>
    </head>
    <body>
        
        <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <div>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a class="navbar-brand" href="/notix/index.jsp">
                            <img src="/notix/img/notixlogo.png" alt="" width="30" height="24" class="d-inline-block align-text-top">
                            Notix
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/notix/cursos/index.jsp">Cursos</a>
                    </li>     
                    <li class="nav-item">
                        <a class="nav-link active" href="/notix/asignaturas/index.jsp">Asignaturas</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/notix/estudiantes/index.jsp">Estudiantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/docentes/index.jsp">Docentes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/notas/index.jsp">Notas</a>
                    </li>
                    <span class="navbar-text">
                        Bienvenidos al Sistema Notix     
                    </span>
                </ul>
            </div>
        </nav>
        
        <h1 align="center">Listado de Asignaturas</h1>
        
        <div class="container-fluid" ng-app = "notix" ng-controller = "asignaturasController as cn" ng-init="cn.listarAsignaturas()">
            <div class="col-12">
                <button  class="btn btn-success col-3" ng-click="cn.abrirNuevo()">Nueva Asignatura</button>
            </div>
            <div class="col-12 table-responsive-xl">
                <table class="table table-striped table-hover">  
                    <thead class="thead-dark">
                        <tr>  
                            <th>Id</th>  
                            <th>Nombre</th>
                            <th>Docente</th>
                            <th>Curso</th>
                            <th>Acción</th>
                        </tr>  
                    </thead>

                    <tr ng-repeat = "asignatura in cn.asignaturas">  
                        <td>{{ asignatura.id}}</td>  
                        <td>{{ asignatura.nombre}}</td>
                        <td>{{ asignatura.docente}}</td>
                        <td>{{ asignatura.curso}}</td>
                        <td><button  class="btn btn-warning" ng-click="cn.abrirEditar(asignatura.id, asignatura.nombre, asignatura.docente, asignatura.curso);">Editar</button>
                            <button  class="btn btn-danger" ng-click="cn.eliminarAsignatura(asignatura.id)">Eliminar</button>
                        </td>
                    </tr>  
                </table> 
            </div>
        </div>
    </body>
</html>
