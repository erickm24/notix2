<%-- 
    Document   : Archivo de peticiones
    Created on : dd/mm/yyyy, hh:mm: AM/PM
    Author     : nombre autor

--%>


<%@page import="java.lang.System"%>
<%@page import="modelo.Notas"%>
<%@page import="modelo.Estudiantes"%>
<%@page import="modelo.Docentes"%>
<%@page import="modelo.Asignaturas"%>
<%@page import="modelo.Cursos"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="application/json;charset=iso-8859-1" language="java" pageEncoding="iso-8859-1" session="true"%>

<%    // Iniciando respuesta JSON.
    String respuesta = "{";

    //Lista de procesos o tareas a realizar 
    List<String> tareas = Arrays.asList(new String[]{
        "actualizarnota",
        "eliminarnota",
        "listarnotas",
        "listardocentes",
        "listarasignaturas",
        "guardarnotas",
        "listarestudiantes"
    });
    
    String proceso = "" + request.getParameter("proceso");
    String doble = "" + request.getParameter("doble");
    Notas c = new Notas();
    Cursos cu = new Cursos();
    Docentes doc = new Docentes();
    Asignaturas asi = new Asignaturas();
    Estudiantes est = new Estudiantes();
    // Validaci�n de par�metros utilizados en todos los procesos.
    if (tareas.contains(proceso)) {
        respuesta += "\"ok\": true,";
        // ------------------------------------------------------------------------------------- //
        // -----------------------------------INICIO PROCESOS----------------------------------- //
        // ------------------------------------------------------------------------------------- //
        if (proceso.equals("guardarnotas")) {
            String estudiante = request.getParameter("estudiante");
            String asignatura = request.getParameter("asignatura");
            String nota1 = request.getParameter("nota1");
            String nota2 = request.getParameter("nota2");
            if(nota2 == null){
                nota2 = "";
            }
            String nota3 = request.getParameter("nota3");   
            if(nota3 == null){
                nota3 = "";
            }

            c.crearNotas(estudiante, asignatura, nota1, nota2, nota3);
            
            if(c.existe()){
                respuesta += "\"" + doble + "\": true";
            }else{
                
                if (c.guardarNotas()) {
                    respuesta += "\"" + proceso + "\": true";
                } else {
                    respuesta += "\"" + proceso + "\": false";
                }
            
            }

        } else if (proceso.equals("eliminarnota")) {
            String id = request.getParameter("id");
            //su codigo ac�
            if (c.borrarNota(id)) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }

        } else if (proceso.equals("listarasignaturas")) {
            //su codigo ac�
            try {
                List<Asignaturas> lista;
                if(request.getParameter("docente").equals("")){
                    lista = asi.getListaAsignaturas();
                }else{
                    lista = asi.getListaAsignaturas(request.getParameter("docente"));
                }
                respuesta += "\"" + proceso + "\": true,\"Asignaturas\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Asignaturas\":[]";
                Logger.getLogger(Asignaturas.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("listardocentes")) {
            //su codigo ac�
            try {
                List<Docentes> lista = doc.getListaDocentes();
                respuesta += "\"" + proceso + "\": true,\"Docentes\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Docentes\":[]";
                Logger.getLogger(Docentes.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("listarestudiantes")) {
            //su codigo ac�
            try {
                List<Estudiantes> lista;
                if(request.getParameter("asignatura").equals("")){
                    lista = est.getListaEstudiantes();
                }else{
                    lista = est.getListaEstudiantes(request.getParameter("asignatura"));                   
                }
                respuesta += "\"" + proceso + "\": true,\"Estudiantes\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Estudiantes\":[]";
                Logger.getLogger(Estudiantes.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("listarnotas")) {
            //su codigo ac�
            try {
                List<Notas> lista = c.getListaNotas();
                respuesta += "\"" + proceso + "\": true,\"Notas\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Notas\":[]";
                Logger.getLogger(Notas.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("actualizarnota")) {
            String id = request.getParameter("id");
            String asignatura = request.getParameter("asignatura");
            String estudiante = request.getParameter("estudiante");
            String nota1 = request.getParameter("nota1");
            String nota2 = request.getParameter("nota2");
            String nota3 = request.getParameter("nota3");
            c.crearNotas(id, estudiante, asignatura, nota1, nota2, nota3);
            if (c.actualizarNotas()) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }
        }

        // ------------------------------------------------------------------------------------- //
        // -----------------------------------FIN PROCESOS-------------------------------------- //
        // ------------------------------------------------------------------------------------- //
        // Proceso desconocido.
    } else {
        respuesta += "\"ok\": false,";
        respuesta += "\"error\": \"INVALID\",";
        respuesta += "\"errorMsg\": \"Lo sentimos, los datos que ha enviado,"
                + " son inv�lidos. Corrijalos y vuelva a intentar por favor.\"";
    }
    // Usuario sin sesi�n.
    // Responder como objeto JSON codificaci�n ISO 8859-1.
    respuesta += "}";
    response.setContentType("application/json;charset=iso-8859-1");
    out.print(respuesta);
%>
