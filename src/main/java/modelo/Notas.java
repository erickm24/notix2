/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;

/**
 *
 * @author siste
 */

public class Notas {

    private int id;
    private Double nota1;
    private Double nota2;
    private Double nota3;
    private Double definitiva;
    private int asignaturasid;
    private String estudiantesid;
    private String asignatura;
    private String estudiante;
    private Boolean notificacion1;
    private Boolean notificacion2;
    private Boolean notificacion3;

    public void setNotificacion1(Boolean notificacion1) {
        this.notificacion1 = notificacion1;
    }

    public void setNotificacion2(Boolean notificacion2) {
        this.notificacion2 = notificacion2;
    }

    public void setNotificacion3(Boolean notificacion3) {
        this.notificacion3 = notificacion3;
    }

    public Boolean getNotificacion1() {
        return notificacion1;
    }

    public Boolean getNotificacion2() {
        return notificacion2;
    }

    public Boolean getNotificacion3() {
        return notificacion3;
    }
    

    public int getAsignaturasid() {
        return asignaturasid;
    }

    public String getEstudiantesid() {
        return estudiantesid;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setAsignaturasid(int asignaturasid) {
        this.asignaturasid = asignaturasid;
    }

    public void setEstudiantesid(String estudiantesid) {
        this.estudiantesid = estudiantesid;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    public Notas() {
    }

    public Notas(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getNota1() {
        return nota1;
    }

    public void setNota1(Double nota1) {
        this.nota1 = nota1;
    }

    public Double getNota2() {
        return nota2;
    }

    public void setNota2(Double nota2) {
        this.nota2 = nota2;
    }

    public Double getNota3() {
        return nota3;
    }

    public void setNota3(Double nota3) {
        this.nota3 = nota3;
    }

    public Double getDefinitiva() {
        return definitiva;
    }

    public void setDefinitiva(Double definitiva) {
        this.definitiva = definitiva;
    }

    public int getAsignaturas() {
        return asignaturasid;
    }

    public void setAsignaturas(int asignaturas) {
        this.asignaturasid = asignaturas;
    }

    public String getEstudiantes() {
        return estudiantesid;
    }

    public void setEstudiantes(String estudiantes) {
        this.estudiantesid = estudiantes;
    }
    
    public List<Notas> getListaNotas() throws SQLException{
        List<Notas> notas = new ArrayList<>();
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from vistanotas order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        Notas c = new Notas();
        while (rs.next()) {
            c = new Notas();
            c.setId(rs.getInt("id"));
            c.setAsignatura(rs.getString("asignatura"));
            c.setEstudiante(rs.getString("estudiante"));
            c.setNota1(rs.getDouble("nota_1"));
            c.setNota2(rs.getDouble("nota_2"));
            c.setNota3(rs.getDouble("nota_3"));
            c.setNotificacion1(rs.getBoolean("notificacion_1"));
            c.setNotificacion2(rs.getBoolean("notificacion_2"));
            c.setNotificacion3(rs.getBoolean("notificacion_3"));
            c.setDefinitiva(rs.getDouble("definitiva"));
            notas.add(c);

        }
        

        conexion.cerrarConexion();
        
        
        return notas;
    }
    
    public Notas getListaNotasById(int id) throws SQLException{
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from vistanotas where id ='"+id+"' order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        Notas c = new Notas();
        while (rs.next()) {
            c = new Notas();
            c.setId(rs.getInt("id"));
            c.setAsignatura(rs.getString("asignatura"));
            c.setEstudiante(rs.getString("estudiante"));
            c.setNota1(rs.getDouble("nota_1"));
            c.setNota2(rs.getDouble("nota_2"));
            c.setNota3(rs.getDouble("nota_3"));
            c.setNotificacion1(rs.getBoolean("notificacion_1"));
            c.setNotificacion2(rs.getBoolean("notificacion_2"));
            c.setNotificacion3(rs.getBoolean("notificacion_3"));
            c.setDefinitiva(rs.getDouble("definitiva"));

        }
        

        conexion.cerrarConexion();
        
        
        return c;
    }
    
    public boolean existe() throws SQLException{
        boolean existe = false;
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from notas where estudiantes_id = '" + this.estudiantesid + 
                "' and asignaturas_id = '" + this.asignaturasid + "';";
        System.out.println(sql);
        ResultSet rs = conexion.consultarBD(sql);
        if (rs.next()) {
            System.out.println(""+rs.getString("estudiantes_id"));
            existe = true;
        }
        conexion.cerrarConexion();
        System.err.println(""+existe);
        return existe;
    }
    
    public void crearNotas(String estudiante, String asignatura, String nota1, String nota2, String nota3){
        this.asignaturasid = Integer.parseInt(asignatura);
        this.estudiantesid = estudiante;
        if(nota1.length()>0){
            this.nota1 = Double.parseDouble(nota1);
        }
        if(nota2.length()>0){
            this.nota2 = Double.parseDouble(nota2);
        }
        if(nota3.length()>0){
            this.nota3 = Double.parseDouble(nota3);
        }
    }
    
        public void crearNotas(String id, String estudiante, String asignatura, String nota1, String nota2, String nota3){
        
            this.id = Integer.parseInt(id);
            this.asignaturasid = Integer.parseInt(asignatura);
            this.estudiantesid = estudiante;
            if(nota1.length()>0){
                this.nota1 = Double.parseDouble(nota1);
            }
            if(nota2.length()>0){
                this.nota2 = Double.parseDouble(nota2);
            }
            if(nota3.length()>0){
                this.nota3 = Double.parseDouble(nota3);
            }
        }
    
    public boolean guardarNotas() throws SQLException {
        System.out.println("bien");
        ConexionBD conexion = new ConexionBD();
        String sentencia = "INSERT INTO notas(estudiantes_id, asignaturas_id, nota_1, nota_2, nota_3) "
                + " VALUES ( '" + this.estudiantesid + "','" + this.asignaturasid +"'," + this.nota1 + "," + this.nota2 + "," + this.nota3 + ");";
        System.out.println(sentencia);
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.insertarBD(sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                Notificar();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean actualizarNotas() throws SQLException {
        ConexionBD conexion = new ConexionBD();
        Notas notavieja = getNotasById(this.id);
        String Sentencia = "UPDATE notas SET estudiantes_id='" + this.estudiantesid + 
                "', asignaturas_id = '" + this.asignaturasid + 
                "', nota_1 = '" + this.nota1 + 
                "', nota_2 = '" + this.nota2 + 
                "', nota_3 = '" + this.nota3 +
                "' WHERE id='" + this.id + "';";
        System.out.println(Sentencia);
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                Notificar(notavieja);
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    private void Notificar(Notas notavieja) throws SQLException{
        
        boolean not1 = false;
        boolean not2 = false;
        boolean not3 = false;
        
        Notas nota = getListaNotasById(id);
        Asignaturas a = new Asignaturas().getListaAsignaturasById(this.asignaturasid);
        Estudiantes est = new Estudiantes();
        est = est.getEstudiantesById(this.estudiantesid);
        String destinatario = est.getEmailAcudiente();
        
        if(!notavieja.getNota1().equals(this.nota1)){
            not1 = true;
        }
        if(!notavieja.getNota2().equals(this.nota2)){
            not2 = true;
        }
        if(!notavieja.getNota3().equals(this.nota3)){
            not3 = true;
        }
        
        String mensaje = "SISTEMA NOTIX\n\nNotificación de Notas\n\n";
        mensaje = mensaje + "Sr. " +est.getNombreAcudiente()+ "\nLas Notas para la asignatura "+a.getNombre()+" de su Acudido: "+nota.estudiante+" han sido actualizadas asi:\n\n";
        
        if(not1){
            mensaje = mensaje + "Nota 1: Antes = "+notavieja.nota1+" Ahora = "+this.nota1+"\n\n";
        }
        
        if(not2){
            mensaje = mensaje + "Nota 2: Antes = "+notavieja.nota2+" Ahora = "+this.nota2+"\n\n";
        }
        
        if(not3){
            mensaje = mensaje + "Nota 3: Antes = "+notavieja.nota3+" Ahora = "+this.nota3+"\n\n";
        }
                
        enviarCorreo correo = new enviarCorreo();
        correo.enviarConGMail(destinatario, "Notificacion de Notas", ""+ mensaje);
        
    }
    
    private void Notificar() throws SQLException{
        
        boolean not1 = false;
        boolean not2 = false;
        boolean not3 = false;
        
        Asignaturas a = new Asignaturas().getListaAsignaturasById(this.asignaturasid);
        Estudiantes est = new Estudiantes();
        est = est.getEstudiantesById(this.estudiantesid);
        String destinatario = est.getEmailAcudiente();
        
        System.out.println("Ahora1: " + this.nota1);
        System.out.println("Ahora2: " + this.nota2);
        System.out.println("Ahora1: " + this.nota3);
        
        if(this.nota1 != null){
            not1 = true;
        }
        if(this.nota2 != null){
            not2 = true;
        }
        if(this.nota3 != null){
            not3 = true;
        }
        
        String mensaje = "SISTEMA NOTIX\n\nNotificación de Notas\n\n";
        mensaje = mensaje + "Sr. " +est.getNombreAcudiente()+ "\nLas Notas para la asignatura "+a.getNombre()+" de su Acudido: "+est.getNombre()+" han sido actualizadas asi:\n\n";
        
        if(not1){
            mensaje = mensaje + "Nota 1: "+this.nota1+"\n\n";
        }
        
        if(not2){
            mensaje = mensaje + "Nota 2: "+this.nota2+"\n\n";
        }
        
        if(not3){
            mensaje = mensaje + "Nota 3: "+this.nota3+"\n\n";
        }
                
        enviarCorreo correo = new enviarCorreo();
        correo.enviarConGMail(destinatario, "Notificacion de Notas", ""+ mensaje);

    }
    
    public boolean borrarNota(String id) {
        String Sentencia = "DELETE FROM notas WHERE id=" + id;
        ConexionBD conexion = new ConexionBD();
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public Notas getNotasById(int id) throws SQLException{
        Notas c = new Notas();
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from notas where id = '" + id + "';";
        System.out.println(sql);
        ResultSet rs = conexion.consultarBD(sql);
        while (rs.next()) {
            c.setId(rs.getInt("id"));
            c.setAsignatura(rs.getString("asignaturas_id"));
            c.setEstudiante(rs.getString("estudiantes_id"));
            c.setNota1(rs.getDouble("nota_1"));
            c.setNota2(rs.getDouble("nota_2"));
            c.setNota3(rs.getDouble("nota_3"));
            c.setNotificacion1(rs.getBoolean("notificacion_1"));
            c.setNotificacion2(rs.getBoolean("notificacion_2"));
            c.setNotificacion3(rs.getBoolean("notificacion_3"));
            c.setDefinitiva(rs.getDouble("definitiva"));
        }
        conexion.cerrarConexion();
        
        return c;
    }

}
