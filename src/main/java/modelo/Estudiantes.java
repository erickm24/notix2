/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;

/**
 *
 * @author siste
 */

public class Estudiantes {

    private String id;
    private String nombre;
    private String nombreAcudiente;
    private String emailEstudiante;
    private String telefonoEstudiante;
    private String telefonoAcudiente;
    private String emailAcudiente;
    private int cursosId;
    private String curso;

    public Estudiantes() {
    }

    public Estudiantes(String id) {
        this.id = id;
    }

    public Estudiantes(String cedula, String nombre, String nombreAcudiente, String emailAcudiente) {
        this.id = cedula;
        this.nombre = nombre;
        this.nombreAcudiente = nombreAcudiente;
        this.emailAcudiente = emailAcudiente;
    }

    public void setCursosId(int cursosId) {
        this.cursosId = cursosId;
    }

    public int getCursosId() {
        return cursosId;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public Estudiantes(String cedula, String nombre, String nombreAcudiente, String emailEstudiante, String telefonoEstudiante, String telefonoAcudiente, String emailAcudiente, int cursosId) {
        this.id = cedula;
        this.nombre = nombre;
        this.nombreAcudiente = nombreAcudiente;
        this.emailEstudiante = emailEstudiante;
        this.telefonoEstudiante = telefonoEstudiante;
        this.telefonoAcudiente = telefonoAcudiente;
        this.emailAcudiente = emailAcudiente;
        this.cursosId = cursosId;
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreAcudiente() {
        return nombreAcudiente;
    }

    public void setNombreAcudiente(String nombreAcudiente) {
        this.nombreAcudiente = nombreAcudiente;
    }

    public String getEmailEstudiante() {
        return emailEstudiante;
    }

    public void setEmailEstudiante(String emailEstudiante) {
        this.emailEstudiante = emailEstudiante;
    }

    public String getTelefonoEstudiante() {
        return telefonoEstudiante;
    }

    public void setTelefonoEstudiante(String telefonoEstudiante) {
        this.telefonoEstudiante = telefonoEstudiante;
    }

    public String getTelefonoAcudiente() {
        return telefonoAcudiente;
    }

    public void setTelefonoAcudiente(String telefonoAcudiente) {
        this.telefonoAcudiente = telefonoAcudiente;
    }

    public String getEmailAcudiente() {
        return emailAcudiente;
    }

    public void setEmailAcudiente(String emailAcudiente) {
        this.emailAcudiente = emailAcudiente;
    }

    public int getCursos() {
        return cursosId;
    }

    public void setCursos(int cursos) {
        this.cursosId = cursos;
    }
    
    public List<Estudiantes> getListaEstudiantes() throws SQLException{
        List<Estudiantes> estudiantes = new ArrayList<>();
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from vistaestudiantes order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        Estudiantes c;
        while (rs.next()) {
            c = new Estudiantes();
            c.setId(rs.getString("id"));
            c.setNombre(rs.getString("nombre"));
            c.setEmailEstudiante(rs.getString("email_estudiante"));
            c.setTelefonoEstudiante(rs.getString("telefono_estudiante"));
            c.setNombreAcudiente(rs.getString("nombre_acudiente"));
            c.setTelefonoAcudiente(rs.getString("telefono_acudiente"));
            c.setEmailAcudiente(rs.getString("email_acudiente"));
            c.setCurso(rs.getString("curso"));
            estudiantes.add(c);

        }
        conexion.cerrarConexion();
        
        
        return estudiantes;
    }
    
    public List<Estudiantes> getListaEstudiantes(String asignatura) throws SQLException{
        List<Estudiantes> estudiantes = new ArrayList<>();
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select id, nombre from estudiantes where cursos_id = (SELECT cursos_id FROM notixdb.asignaturas where id = "+asignatura+") order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        System.out.println(sql);
        Estudiantes c;
        while (rs.next()) {
            c = new Estudiantes();
            c.setId(rs.getString("id"));
            c.setNombre(rs.getString("nombre"));
            estudiantes.add(c);

        }
        conexion.cerrarConexion();
        
        
        return estudiantes;
    }
    
    public Estudiantes getEstudiantesById(String id) throws SQLException{
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from estudiantes where id = '"+id+"' order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        System.out.println(sql);
        Estudiantes c = new Estudiantes();
        while (rs.next()) {
            c.setId(rs.getString("id"));
            c.setNombre(rs.getString("nombre"));
            c.setNombreAcudiente(rs.getString("nombre_acudiente"));
            c.setEmailAcudiente(rs.getString("email_acudiente"));

        }
        conexion.cerrarConexion();
        
        
        return c;
    }
    
    public void crearEstudiante(String cedula, String nombre, String emailEstudiante, String telefonoEstudiante, String nombreAcudiente, String emailAcudiente, String telefonoAcudiente, String cursosId) {
        this.id = cedula;
        this.nombre = nombre;
        this.emailEstudiante = emailEstudiante;
        this.telefonoEstudiante = telefonoEstudiante;
        this.nombreAcudiente = nombreAcudiente;
        this.emailAcudiente = emailAcudiente;
        this.telefonoAcudiente = telefonoAcudiente;
        this.cursosId = Integer.parseInt(cursosId);
        
    }
    
    public boolean guardarEstudiante() {
        System.out.println("bien");
        ConexionBD conexion = new ConexionBD();
        String sentencia = "INSERT INTO estudiantes(id, nombre, email_estudiante, telefono_estudiante, nombre_acudiente, email_acudiente, telefono_acudiente, cursos_id) "
                + " VALUES ( '" + this.id + "','" + this.nombre +"','" + this.emailEstudiante + "','" + this.telefonoEstudiante + "','" + this.nombreAcudiente + "','" + this.emailAcudiente + "','" + this.telefonoAcudiente + "'," + this.cursosId + ");";
        System.out.println(sentencia);
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.insertarBD(sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean borrarEstudiante(String id) {
        String Sentencia = "DELETE FROM estudiantes WHERE id='" + id + "'";
        ConexionBD conexion = new ConexionBD();
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean actualizarEstudiante() {
        ConexionBD conexion = new ConexionBD();
        String Sentencia = "UPDATE estudiantes SET nombre='" + this.nombre + 
                "', email_estudiante = '" + this.emailEstudiante + 
                "', telefono_estudiante = '" + this.telefonoEstudiante + 
                "', nombre_acudiente = '" + this.nombreAcudiente + 
                "', telefono_acudiente = '" + this.telefonoAcudiente +
                "', email_acudiente = '" + this.emailAcudiente +
                "', cursos_id = '" + this.cursosId + 
                "' WHERE id='" + this.id + "';";
        System.out.println(Sentencia);
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
}
