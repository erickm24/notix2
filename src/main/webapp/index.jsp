<!DOCTYPE html>
<html>
    <head>
        <title>Notix</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
       
    </head>
    <style type="text/css">
        .messages {
            color: #FA787E;
        }
        form.ng-submitted input.ng-invalid{
            border-color: #FA787E;
        }
        form input.ng-invalid.ng-touched {
            border-color: #FA787E;
        }
    </style>
    <body>
 
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <div>
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <a class="navbar-brand" href="/notix/index.jsp">
                        <img src="/notix/img/notixlogo.png" alt="" width="30" height="24" class="d-inline-block align-text-top">
                        Notix
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/notix/cursos/index.jsp">Cursos</a>
                </li>     
                <li class="nav-item">
                    <a class="nav-link " href="/notix/asignaturas/index.jsp">Asignaturas</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/notix/estudiantes/index.jsp">Estudiantes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/notix/docentes/index.jsp">Docentes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/notix/notas/index.jsp">Notas</a>
                </li>
                <span class="navbar-text">
                    Bienvenidos al Sistema Notix     
                </span>
            </ul>
        </div>
    </nav>  
        
        <img src="/notix/img/NOTIX.png" width="900" height="300">

    </body>

</html>

