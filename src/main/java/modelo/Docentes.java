/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;
/**
 *
 * @author siste
 */

//Este lo hará Sebastián

public class Docentes {

    private String cedula;
    private String nombre;
    private String telefono;
    private String email;

    public Docentes() {
    }

    public Docentes(String cedula, String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
    }
    
    public Docentes(String cedula, String nombre, String telefono, String email) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void crearDocente(String cedula, String nombre, String telefono, String email){
        this.cedula = cedula;
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
    }

    public List<Docentes> getListaDocentes() throws SQLException{
        List<Docentes> docentes = new ArrayList<>();
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from docentes order by nombre asc";
        ResultSet rs = conexion.consultarBD(sql);
        Docentes c;
        while (rs.next()) {
            c = new Docentes();
            c.setCedula(rs.getString("cedula"));
            c.setNombre(rs.getString("nombre"));
            c.setTelefono(rs.getString("telefono"));
            c.setEmail(rs.getString("email"));
            docentes.add(c);

        }
        conexion.cerrarConexion();
        
        
        return docentes;
    }
    
    public boolean actualizarDocente() {
        ConexionBD conexion = new ConexionBD();
        String Sentencia = "UPDATE docentes SET cedula='" + this.cedula +  "', nombre='" + this.nombre +  "'telefono='" + this.telefono +  "'email='" + this.email +  "' WHERE cedula=" + this.cedula + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean borrarDocente(String cedula) {
        String Sentencia = "DELETE FROM docentes WHERE cedula='" +cedula + "'";
        ConexionBD conexion = new ConexionBD();
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean guardarDocente() {
        System.out.println("bien");
        ConexionBD conexion = new ConexionBD();
        String sentencia = "INSERT INTO docentes(cedula,nombre,email,telefono) "
                + " VALUES ('" + this.cedula + "','" + this.nombre + "','" + this.email + "','" + this.telefono + "');";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.insertarBD(sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
}
