angular.module('notix', [])
                .controller('estudiantesController', ['$scope', function ($scope) {
                        $scope.user = {};

                        $scope.update = function () {
                            console.log($scope.user);
                        };

                        $scope.reset = function (form) {
                            $scope.user = {};
                            if (form) {
                                form.$setPristine();
                                form.$setUntouched();
                            }
                        };

                        $scope.reset();
                    }]);
        var app = angular.module('notix', []);
        app.controller('estudiantesController', ['$http', controladorEstudiantes]);
        //
        function validar() {
            return true;
        }
        function controladorEstudiantes($http) {
            var cn = this;
            var id = "";

            cn.getGET = function ()
            {
                cn.id = 0;
                // capturamos la url
                var loc = document.location.href;
                // si existe el interrogante
                if(loc.indexOf('?')>0)
                {
                    // cogemos la parte de la url que hay despues del interrogante
                    var getString = loc.split('?')[1];
                    // obtenemos un array con cada clave=valor
                    var GET = getString.split('&');
                    var get = {};
                    // recorremos todo el array de valores
                    for(var i = 0, l = GET.length; i < l; i++){
                        var tmp = GET[i].split('=');
                        get[tmp[0]] = unescape(decodeURI(tmp[1]));
                    }
  
                    cn.id = get;
                    
                var url = "PeticionesEstudiantes.jsp";
                var params = {
                    proceso: "listarcursos"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesEstudiantes.jsp',
                    params: params
                }).then(function (res) {
                    cn.cursos = res.data.Cursos;
                    var combocurso = document.getElementById("curso");
                    var option;
                    for(curso of res.data.Cursos){
                        option = document.createElement('option');
                        option.text = curso.nombre;
                        if(option.text == get.curso){
                            option.selected = true;
                        }
                        option.value = curso.id;
                        combocurso.add(option);
                    }
                    
                });

                    return get;
                }
            };

            cn.abrirEditar = function ($estudiante) {
                window.location.href="editar.jsp?id="+$estudiante.id+"&nombre="+$estudiante.nombre+"&email_estudiante="+$estudiante.emailEstudiante+"&telefono_estudiante="+$estudiante.telefonoEstudiante+"&nombre_acudiente="+$estudiante.nombreAcudiente+"&telefono_acudiente="+$estudiante.telefonoAcudiente+"&email_acudiente="+$estudiante.emailAcudiente+"&curso="+$estudiante.curso;
            };
            
            cn.abrirNuevo = function () {
                window.location.href="crear.jsp";
            };
            
            cn.abrirLista = function () {
                window.location.href="index.jsp";
            };

            cn.listarEstudiantes = function () {
                var url = "PeticionesEstudiantes.jsp";
                var params = {
                    proceso: "listarestudiantes"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesEstudiantes.jsp',
                    params: params
                }).then(function (res) {
                    cn.estudiantes = res.data.Estudiantes;
                });
            };
            
            cn.listarCursos = function () {
                var url = "PeticionesEstudiantes.jsp";
                var params = {
                    proceso: "listarcursos"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesEstudiantes.jsp',
                    params: params
                }).then(function (res) {
                    cn.cursos = res.data.Cursos;
                    var combocurso = document.getElementById("curso");
                    for(curso of res.data.Cursos){
                        var option = document.createElement('option');
                        option.text = curso.nombre;
                        option.value = curso.id;
                        combocurso.add(option);
                        
                    }
                });
                
                return cn.cursos;
            };
            
            cn.guardarEstudiante = function () {
                var estudiante = {
                    proceso: "guardarestudiante",
                    id: cn.identificacion,
                    nombre: cn.nombre,
                    emailestudiante: cn.email_estudiante,
                    telefonoestudiante: cn.telefono_estudiante,
                    nombreacudiente: cn.nombre_acudiente,
                    emailacudiente: cn.email_acudiente,
                    telefonoacudiente: cn.telefono_acudiente,
                    curso: ""+document.getElementById("curso").value
                };
                
                $http({
                    method: 'POST',
                    url: 'PeticionesEstudiantes.jsp',
                    params: estudiante
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[estudiante.proceso] === true) {
                            alert("Guardado con éxito");
                            //                                                            cn.listarContactos();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            cn.eliminarEstudiante = function ($id) {
                var estudiante = {
                    proceso: "eliminarestudiante",
                    id: $id
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesEstudiantes.jsp',
                    params: estudiante
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[estudiante.proceso] === true) {
                            alert("Eliminado con éxito");
                            cn.listarEstudiantes();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            
            cn.actualizarEstudiante = function () {

                var estudiante = {
                    proceso: "actualizarestudiante",
                    id: document.getElementById("id").value,
                    nombre: document.getElementById("nombre").value,
                    emaile: document.getElementById("email_estudiante").value,
                    telefonoe: document.getElementById("telefono_estudiante").value,
                    nombrea: document.getElementById("nombre_acudiente").value,
                    telefonoa: document.getElementById("telefono_acudiente").value,
                    emaila: document.getElementById("email_acudiente").value,
                    curso: document.getElementById("curso").value
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesEstudiantes.jsp',
                    params: estudiante
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[estudiante.proceso] === true) {
                            alert("Actualizar estudiante con éxito");
                            cn.abrirLista();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };

        }