/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;


/**
 *
 * @author siste
 */

public class Cursos {

    private int id;
    private String nombre;

    public Cursos() {
    }

    public Cursos(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void crearCurso(int id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }
    
    public void crearCurso(String nombre){
        this.nombre = nombre;
    }
    
    public boolean guardarCurso() {
        System.out.println("bien");
        ConexionBD conexion = new ConexionBD();
        String sentencia = "INSERT INTO cursos(nombre) "
                + " VALUES ( '" + this.nombre + "');";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.insertarBD(sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public List<Cursos> getListaCursos() throws SQLException{
        List<Cursos> cursos = new ArrayList<>();
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from cursos order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        Cursos c;
        while (rs.next()) {
            c = new Cursos();
            c.setId(rs.getInt("id"));
            c.setNombre(rs.getString("nombre"));
            cursos.add(c);

        }
        conexion.cerrarConexion();
        
        
        return cursos;
    }
    
    public List<String> getListaCursosNombres() throws SQLException{
        List<String> cursos = new ArrayList<>();
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select nombre from cursos order by nombre asc";
        ResultSet rs = conexion.consultarBD(sql);
        String c;
        while (rs.next()) {
            c = ""+(rs.getString("nombre"));
            cursos.add(c);

        }
        conexion.cerrarConexion();
        
        
        return cursos;
    }
    
    public boolean actualizarCurso() {
        ConexionBD conexion = new ConexionBD();
        String Sentencia = "UPDATE cursos SET nombre='" + this.nombre +  "' WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean borrarCurso(int id) {
        String Sentencia = "DELETE FROM cursos WHERE id='" + id + "'";
        ConexionBD conexion = new ConexionBD();
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public int getIdByName(String nombre) throws SQLException{
        
        int id = -1;
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select id from cursos where nombre = '" + nombre + "';";
        ResultSet rs = conexion.consultarBD(sql);
        while (rs.next()) {
            id = rs.getInt("id");

        }
        conexion.cerrarConexion();
        
        
        return id;
        
    }
    
}
