angular.module('notix', [])
                .controller('docentesController', ['$scope', function ($scope) {
                        $scope.user = {};

                        $scope.update = function () {
                            console.log($scope.user);
                        };

                        $scope.reset = function (form) {
                            $scope.user = {};
                            if (form) {
                                form.$setPristine();
                                form.$setUntouched();
                            }
                        };

                        $scope.reset();
                    }]);
        var app = angular.module('notix', []);
        app.controller('docentesController', ['$http', controladorDocentes]);
        //
        function validar() {
            return true;
        }
        function controladorDocentes($http) {
            var cn = this;
            var id = "";

            cn.getGET = function ()
            {
                cn.id = 0;
                // capturamos la url
                var loc = document.location.href;
                // si existe el interrogante
                if(loc.indexOf('?')>0)
                {
                    // cogemos la parte de la url que hay despues del interrogante
                    var getString = loc.split('?')[1];
                    // obtenemos un array con cada clave=valor
                    var GET = getString.split('&');
                    var get = {};
                    // recorremos todo el array de valores
                    for(var i = 0, l = GET.length; i < l; i++){
                        var tmp = GET[i].split('=');
                        get[tmp[0]] = unescape(decodeURI(tmp[1]));
                    }
                    cn.id = get;
                    return get;
                }
            };

            cn.abrirEditar = function ($cedula, $nombre, $telefono, $email) {
                window.location.href="editar.jsp?cedula="+$cedula+"&nombre="+$nombre+"&telefono="+$telefono+"&email="+$email;
            };
            
            cn.abrirNuevo = function () {
                window.location.href="crear.jsp";
            };
            
            cn.abrirLista = function () {
                window.location.href="index.jsp";
            };

            cn.listarDocente = function () {
                var url = "PeticionesDocentes.jsp";
                var params = {
                    proceso: "listardocentes"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesDocentes.jsp',
                    params: params
                }).then(function (res) {
                    cn.docentes = res.data.Docentes;
                });
            };
            cn.guardarDocente = function () {
                var docente = {
                    proceso: "guardardocente",
                    cedula: cn.cedula,
                    nombre: cn.nombre,
                    email: cn.email,
                    telefono: cn.telefono
                    
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesDocentes.jsp',
                    params: docente
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[docente.proceso] === true) {
                            alert("Guardado con éxito");
                            //                                                            cn.listarContactos();
                        } else {
                            alert("Por favor verifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            cn.eliminarDocente = function ($cedula) {
                var docente = {
                    proceso: "eliminardocente",
                    id: $cedula
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesDocentes.jsp',
                    params: docente
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[docente.proceso] === true) {
                            alert("Eliminado con éxito");
                            cn.listarDocente();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            
            cn.actualizarDocente = function () {

                var docente = {
                    proceso: "actualizardocente",
                    cedula: document.getElementById("cedula").value,
                    nombre: document.getElementById("nombre").value,
                    telefono: document.getElementById("telefono").value,
                    email: document.getElementById("email").value
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesDocentes.jsp',
                    params: docente
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[docente.proceso] === true) {
                            alert("Actualizado docente con éxito");
                            cn.abrirLista();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };

        }