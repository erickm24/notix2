angular.module('notix', [])
                .controller('cursosController', ['$scope', function ($scope) {
                        $scope.user = {};

                        $scope.update = function () {
                            console.log($scope.user);
                        };

                        $scope.reset = function (form) {
                            $scope.user = {};
                            if (form) {
                                form.$setPristine();
                                form.$setUntouched();
                            }
                        };

                        $scope.reset();
                    }]);
        var app = angular.module('notix', []);
        app.controller('cursosController', ['$http', controladorCursos]);
        //
        function validar() {
            return true;
        }
        function controladorCursos($http) {
            var cn = this;
            var id = "";

            cn.getGET = function ()
            {
                cn.id = 0;
                // capturamos la url
                var loc = document.location.href;
                // si existe el interrogante
                if(loc.indexOf('?')>0)
                {
                    // cogemos la parte de la url que hay despues del interrogante
                    var getString = loc.split('?')[1];
                    // obtenemos un array con cada clave=valor
                    var GET = getString.split('&');
                    var get = {};
                    // recorremos todo el array de valores
                    for(var i = 0, l = GET.length; i < l; i++){
                        var tmp = GET[i].split('=');
                        get[tmp[0]] = unescape(decodeURI(tmp[1]));
                    }
                    cn.id = get;
                    return get;
                }
            };

            cn.abrirEditar = function ($id, $nombre) {
                window.location.href="editar.jsp?id="+$id+"&nombre="+$nombre;
            };
            
            cn.abrirNuevo = function () {
                window.location.href="crear.jsp";
            };
            
            cn.abrirLista = function () {
                window.location.href="index.jsp";
            };

            cn.listarCursos = function () {
                var url = "PeticionesCursos.jsp";
                var params = {
                    proceso: "listarcursos"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesCursos.jsp',
                    params: params
                }).then(function (res) {
                    cn.cursos = res.data.Cursos;
                });
            };
            cn.guardarCurso = function () {
                var curso = {
                    proceso: "guardarCurso",
                    nombre: cn.nombre
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesCursos.jsp',
                    params: curso
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[curso.proceso] === true) {
                            alert("Guardado con éxito");
                            //                                                            cn.listarContactos();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            cn.eliminarCurso = function ($id) {
                var curso = {
                    proceso: "eliminarcurso",
                    identificacion: $id
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesCursos.jsp',
                    params: curso
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[curso.proceso] === true) {
                            alert("Eliminado con éxito");
                            cn.listarCursos();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            
            cn.actualizarCurso = function () {

                var curso = {
                    proceso: "actualizarcurso",
                    id: document.getElementById("id").value,
                    nombre: document.getElementById("nombre").value
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesCursos.jsp',
                    params: curso
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[curso.proceso] === true) {
                            alert("Actualizar curso con éxito");
                            cn.abrirLista();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };

        }