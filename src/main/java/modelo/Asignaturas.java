/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;

/**
 *
 * @author siste
 */

public class Asignaturas {

 

    private int id;
    private String nombre;
    private String docente;
    private int cursos_id;
    private String docentes_id;
    private String curso;
    
    
    public Asignaturas() {
    }
  
   
    public Asignaturas(int id) {
            this.id = id;
    
    }


    
    public Asignaturas(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    
    
    public Asignaturas(int id, String nombre, int cursosId, String docentesId) {
        this.id = id;
        this.nombre = nombre;
        this.cursos_id = cursosId;
        this.docentes_id = docentesId;
        
    }
    
    
    public Asignaturas(int id, String nombre, String docente, String curso) {
        this.id = id;
        this.nombre = nombre;
        this.docente = docente;
        this.curso = curso;
        
    }
    
   
     public void crearAsignatura(String nombre, int cursosId, String docentesId) {
        
        this.nombre = nombre;
        this.cursos_id = cursosId;
        this.docentes_id = docentesId;
        
         System.out.println(nombre + " ");
         System.out.println(cursosId + " ");
         System.out.println(docentesId + " ");
        
    }
    
     public void crearAsignaturas(int id, String nombre, int cursosId, String docentesId) {
        this.id = id;
        this.nombre = nombre;
        this.cursos_id = cursosId;
        this.docentes_id = docentesId;
        
       
    }
    
    
    public void crearAsignatura(int id, String nombre, String docente, String curso) {
        this.id = id;
        this.nombre = nombre;
        this.docente = docente;
        this.curso = curso;
        
    }
    
    
    public void crearAsignatura(int id) {
        
        this.id = id;
       
        
    }
    
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCursosId() {
        return cursos_id;
    }

    public void setCursosId(int cursosId) {
        this.cursos_id = cursosId;
    }

    public String getDocentesId() {
        return docentes_id;
    }

    public void setDocentesId(String docentesId) {
        this.docentes_id = docentesId;
    }
    

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }   

    public String getCurso() {
        return curso;
    }

    public String getDocente() {
        return docente;
    }
 
    public boolean guardarAsignatura() {
        
        System.out.println("bien");
        
        ConexionBD conexion = new ConexionBD();
        String sentencia = "INSERT INTO asignaturas(nombre,cursos_id,docentes_id) "
                + " VALUES ('" + this.nombre + "','" + this.cursos_id + "','" + this.docentes_id + "');";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.insertarBD(sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public List<Asignaturas> getListaAsignaturas() throws SQLException{
        
        List<Asignaturas> asignaturas = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from vistaasignaturas order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        Asignaturas c;
        while (rs.next()) {
            c = new Asignaturas();
            c.setId(rs.getInt("id"));
            c.setNombre(rs.getString("nombre"));
            c.setDocente(rs.getString("docente"));
            c.setCurso(rs.getString("curso"));
            asignaturas.add(c);
        }
        conexion.cerrarConexion();
        return asignaturas;
    }
    
    public Asignaturas getListaAsignaturasById(int id) throws SQLException{
        
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from vistaasignaturas where id = '"+id+"'order by id asc";
        ResultSet rs = conexion.consultarBD(sql);
        Asignaturas c = new Asignaturas();
        while (rs.next()) {
            c.setId(rs.getInt("id"));
            c.setNombre(rs.getString("nombre"));
            c.setDocente(rs.getString("docente"));
            c.setCurso(rs.getString("curso"));
        }
        conexion.cerrarConexion();
        return c;
    }

    public List<Asignaturas> getListaAsignaturas(String docente) throws SQLException{
        
        List<Asignaturas> asignaturas = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from asignaturas where docentes_id = '"+ docente +"' order by id asc";
        System.out.println(sql);
        
        ResultSet rs = conexion.consultarBD(sql);
        Asignaturas c;
        while (rs.next()) {
            c = new Asignaturas();
            c.setId(rs.getInt("id"));
            c.setNombre(rs.getString("nombre"));
            c.setDocente(rs.getString("docentes_id"));
            c.setCurso(rs.getString("cursos_id"));
            asignaturas.add(c);
        }
        conexion.cerrarConexion();
        return asignaturas;
    }
     
    public List<String> getListaAsignaturasNombres() throws SQLException{
        
        List<String> asignaturas = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "select nombre from asignaturas order by nombre asc";
        ResultSet rs = conexion.consultarBD(sql);
        String c;
        while (rs.next()) {
            c = ""+(rs.getString("nombre"));
            asignaturas.add(c);
        }
        conexion.cerrarConexion();

        return asignaturas;
    }
    
    
    public boolean actualizarAsignatura() {
        ConexionBD conexion = new ConexionBD();
        String Sentencia = "UPDATE asignaturas SET nombre='" + nombre +  "', cursos_id='" + cursos_id +  "', docentes_id='" + docentes_id +  "' WHERE id="+id+";"; 
        System.out.println(Sentencia);
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean borrarAsignatura(int id) {
        
       String Sentencia = "DELETE FROM asignaturas WHERE id= " + id + ";";
       System.out.println(" envio de Sentencia en borrarAsignatura()");
       System.out.println(id);
       
        ConexionBD conexion = new ConexionBD();
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                System.out.println("Borrado");
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                System.out.println("No Borrado");
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
}
