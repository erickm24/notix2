angular.module('notix', [])
                .controller('notasController', ['$scope', function ($scope) {
                        $scope.user = {};

                        $scope.update = function () {
                            console.log($scope.user);
                        };

                        $scope.reset = function (form) {
                            $scope.user = {};
                            if (form) {
                                form.$setPristine();
                                form.$setUntouched();
                            }
                        };

                        $scope.reset();
                    }]);
        var app = angular.module('notix', []);
        app.controller('notasController', ['$http', controladorNotas]);
        //
        function validar() {
            return true;
        }
        function controladorNotas($http) {
            var cn = this;
            var id = "";

            cn.getGET = function ()
            {
                cn.id = 0;
                // capturamos la url
                var loc = document.location.href;
                // si existe el interrogante
                if(loc.indexOf('?')>0)
                {
                    // cogemos la parte de la url que hay despues del interrogante
                    var getString = loc.split('?')[1];
                    // obtenemos un array con cada clave=valor
                    var GET = getString.split('&');
                    var get = {};
                    // recorremos todo el array de valores
                    for(var i = 0, l = GET.length; i < l; i++){
                        var tmp = GET[i].split('=');
                        get[tmp[0]] = unescape(decodeURI(tmp[1]));
                    }
  
                    cn.id = get;
                    
                    //Listar Asignaturas//
                    
                    var url = "PeticionesNotas.jsp";
                    var params = {
                        proceso: "listarasignaturas",
                        docente: ""
                    };
                    $http({
                        method: 'POST',
                        url: 'PeticionesNotas.jsp',
                        params: params
                    }).then(function (res) {
                        cn.asignaturas = res.data.Asignaturas;
                        var comboasignaturas = document.getElementById("asignaturas");
                        for(i= comboasignaturas.length; i>=0; i--){
                            comboasignaturas.remove(i);
                        }
                        for(asignatura of res.data.Asignaturas){
                            var option = document.createElement('option');
                            option.text = asignatura.nombre;
                            option.value = asignatura.id;
                            if(get.asignatura == option.text){
                                option.selected = true;
                            }
                            comboasignaturas.add(option);

                        }
                    });
                    
                    //Listar Estudiante//
                    
                    url = "PeticionesNotas.jsp";
                    params = {
                        proceso: "listarestudiantes",
                        asignatura: ""
                    };
                    $http({
                        method: 'POST',
                        url: 'PeticionesNotas.jsp',
                        params: params
                    }).then(function (res) {
                        cn.estudiantes = res.data.Estudiantes;
                        var comboestudiantes = document.getElementById("estudiantes");
                        for(i= comboestudiantes.length; i>=0; i--){
                            comboestudiantes.remove(i);
                        }
                        for(estudiante of res.data.Estudiantes){
                            var option = document.createElement('option');
                            option.text = estudiante.nombre;
                            option.value = estudiante.id;
                            if(get.estudiante == option.text){
                                option.selected = true;
                            }
                            comboestudiantes.add(option);

                        }
                    });

                    return get;
                }
            };

            cn.abrirEditar = function ($nota) {
                window.location.href="editar.jsp?id="+$nota.id+"&asignatura="+$nota.asignatura+"&estudiante="+$nota.estudiante+"&nota1="+$nota.nota1+"&nota2="+$nota.nota2+"&nota3="+$nota.nota3;
            };
            
            cn.abrirNuevo = function () {
                window.location.href="crear.jsp";
            };
            
            cn.abrirLista = function () {
                window.location.href="index.jsp";
            };

            cn.listarNotas = function () {
                var url = "PeticionesNotas.jsp";
                var params = {
                    proceso: "listarnotas"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesNotas.jsp',
                    params: params
                }).then(function (res) {
                    cn.notas = res.data.Notas;
                });
            };

            cn.listarDocentes = function () {
                var url = "PeticionesNotas.jsp";
                var params = {
                    proceso: "listardocentes"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesNotas.jsp',
                    params: params
                }).then(function (res) {
                    cn.docentes = res.data.Docentes;
                    var combodocentes = document.getElementById("docentes");
                    for(docente of res.data.Docentes){
                        var option = document.createElement('option');
                        option.text = docente.nombre;
                        option.value = docente.cedula;
                        combodocentes.add(option);
                        
                    }
                });
                
                return cn.docentes;
            };
                        
            cn.listarAsignaturas = function () {
                var url = "PeticionesNotas.jsp";
                var params = {
                    proceso: "listarasignaturas",
                    docente: document.getElementById("docentes")[document.getElementById("docentes").selectedIndex].value
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesNotas.jsp',
                    params: params
                }).then(function (res) {
                    cn.asignaturas = res.data.Asignaturas;
                    var comboasignaturas = document.getElementById("asignaturas");
                    for(i= comboasignaturas.length; i>=0; i--){
                        comboasignaturas.remove(i);
                    }
                    for(asignatura of res.data.Asignaturas){
                        var option = document.createElement('option');
                        option.text = asignatura.nombre;
                        option.value = asignatura.id;
                        comboasignaturas.add(option);
                        
                    }
                });
                
                return cn.asignaturas;
            };
                        
            cn.listarEstudiantes = function () {
                var url = "PeticionesNotas.jsp";
                var params = {
                    proceso: "listarestudiantes",
                    asignatura: document.getElementById("asignaturas")[document.getElementById("asignaturas").selectedIndex].value
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesNotas.jsp',
                    params: params
                }).then(function (res) {
                    cn.estudiantes = res.data.Estudiantes;
                    var comboestudiantes = document.getElementById("estudiantes");
                    for(i= comboestudiantes.length; i>=0; i--){
                        comboestudiantes.remove(i);
                    }
                    for(estudiante of res.data.Estudiantes){
                        var option = document.createElement('option');
                        option.text = estudiante.nombre;
                        option.value = estudiante.id;
                        comboestudiantes.add(option);
                        
                    }
                });
                
                return cn.asignaturas;
            };
            
            cn.guardarNotas = function () {
                var notas = {
                    proceso: "guardarnotas",
                    doble: false,
                    estudiante: ""+document.getElementById("estudiantes").value,
                    asignatura: ""+document.getElementById("asignaturas").value,
                    nota1: cn.nota1,
                    nota2: cn.nota2,
                    nota3: cn.nota3
                };
                if(cn.nota1 === undefined || cn.nota2>5 || cn.nota3>5){
                    alert("La nota debe ser ingresada entre 0 y 5: ");
                }else{

                    $http({
                        method: 'POST',
                        url: 'PeticionesNotas.jsp',
                        params: notas
                    }).then(function (res) {
                        if (res.data.ok === true) {
                            if(res.data[notas.doble] === true){
                                alert("Notas ya existen, utilice la opción de Editar");
                            }else{
                                if (res.data[notas.proceso] === true) {
                                    alert("Guardado con éxito");
                                    //                                                            cn.listarContactos();
                                } else {
                                    alert("Por favor vefifique sus datos");
                                }
                            }

                        } else {
                            alert(res.data.errorMsg);
                        }
                    });
                
                }

            };
            cn.eliminarNota = function ($id) {
                var nota = {
                    proceso: "eliminarnota",
                    id: $id
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesNotas.jsp',
                    params: nota
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[nota.proceso] === true) {
                            alert("Eliminado con éxito");
                            cn.listarNotas();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            
            cn.actualizarNotas = function () {

                var notas = {
                    proceso: "actualizarnota",
                    id: document.getElementById("id").value,
                    asignatura: document.getElementById("asignaturas").value,
                    estudiante: document.getElementById("estudiantes").value,
                    nota1: document.getElementById("nota1").value,
                    nota2: document.getElementById("nota2").value,
                    nota3: document.getElementById("nota3").value
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesNotas.jsp',
                    params: notas
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[notas.proceso] === true) {
                            alert("Actualizar Notas con éxito");
                            cn.abrirLista();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };

        }