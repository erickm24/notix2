<%-- 
    Document   : editar
    Created on : 20/09/2021, 4:33:00 p. m.
    Author     : siste
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link id="template-file" href="/index.html" rel="import" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src = "http://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script> 
        <script src = "notasController.js"></script>
        <title>Editar Notas</title>
    </head>
    <body>
        
        <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <div>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a class="navbar-brand" href="/notix/index.jsp">
                            <img src="/notix/img/notixlogo.png" alt="" width="30" height="24" class="d-inline-block align-text-top">
                            Notix
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/notix/cursos/index.jsp">Cursos</a>
                    </li>     
                    <li class="nav-item">
                        <a class="nav-link " href="/notix/asignaturas/index.jsp">Asignaturas</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/notix/estudiantes/index.jsp">Estudiantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/docentes/index.jsp">Docentes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/notix/notas/index.jsp">Notas</a>
                    </li>
                    <span class="navbar-text">
                        Bienvenidos al Sistema Notix     
                    </span>
                </ul>
            </div>
        </nav>  

        
        <div class="container-fluid" ng-app = "notix" ng-controller = "notasController as cn" ng-init="cn.getGET()">
            <form name="userForm">
                <div class="row">
                    <div class="col-12">
                        <center><h1>Actualizar Notas</h1></center> 
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-4" >
                        <label>Id</label>
                        <input id="id" class="form-control" value="{{cn.id.id}}" disabled>
                    </div>
                    <div class="col-4" >
                        <label>Asignatura</label>
                        <select id="asignaturas" class="form-control" disabled></select>
                    </div>
                    <div class="col-4" >
                        <label>Estudiante</label>
                        <select id="estudiantes" class="form-control" disabled></select>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4">
                        <label>Nota 1</label>
                        <input id="nota1" name="nota1" class="form-control" type="number" value="{{cn.id.nota1}}" ng-model-options="{updateOn: 'blur'}"  min="0" max="5" required>
                            <span class="messages" ng-show="userForm.nota1.$error">
                                <span ng-show="userForm.nota1.$error.required">Ingresar la nota</span>
                            </span>
                    </div>
                    <div class="col-4">
                        <label>Nota 2</label>
                        <input id="nota2" name="nota2" class="form-control" type="number" value="{{cn.id.nota2}}" ng-model-options="{updateOn: 'blur'}">
                    </div>
                    <div class="col-4">
                        <label>Nota 3</label>
                        <input id="nota3" name="nota3" class="form-control" type="number" value="{{cn.id.nota3}}" ng-model-options="{updateOn: 'blur'}">
                    </div>
                </div>
                <div><br></div>
                <div class="row">
                    <div class="col-3">
                        <button  class="btn btn-primary" ng-click="cn.actualizarNotas()">Actualizar Notas</button>
                    </div>
                </div>
            </form>
        </div>     
    </body>
</html>
