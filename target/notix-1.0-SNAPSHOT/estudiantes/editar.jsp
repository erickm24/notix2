<%-- 
    Document   : editar
    Created on : 20/09/2021, 4:33:00 p. m.
    Author     : siste
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link id="template-file" href="/index.html" rel="import" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src = "http://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script> 
        <script src = "estudiantesController.js"></script>
        <title>Editar Estudiante</title>
    </head>
    <body>
        
        <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <div>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a class="navbar-brand" href="/notix/index.jsp">
                            <img src="/notix/img/notixlogo.png" alt="" width="30" height="24" class="d-inline-block align-text-top">
                            Notix
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/cursos/index.jsp">Cursos</a>
                    </li>     
                    <li class="nav-item">
                        <a class="nav-link " href="/notix/asignaturas/index.jsp">Asignaturas</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/notix/estudiantes/index.jsp">Estudiantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/docentes/index.jsp">Docentes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/notas/index.jsp">Notas</a>
                    </li>
                    <span class="navbar-text">
                        Bienvenidos al Sistema Notix     
                    </span>
                </ul>
            </div>
        </nav>
        
        <div class="container-fluid" ng-app = "notix" ng-controller = "estudiantesController as cn" ng-init="cn.getGET()">
            <form name="userForm" novalidate>
                <div class="row">
                    <div class="col-12">
                        <center><h1>Modificar Estudiante</h1></center> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <label >Identificacion</label>
                                <input  id="id" name="id" class="form-control" type="number" min="0" value="{{cn.id.id}}" disabled>
                                <span class="messages" ng-show="userForm.identificacion.$error">
                                    <span ng-show="userForm.identificacion.$error.required">El campo es obligatorio.</span>
                                </span>
                            </div>
                            <div class="col-6">
                                <label>Nombre</label>
                                <input id="nombre" name="nombre" class="form-control" type="text" value="{{cn.id.nombre}}" ng-model-options="{updateOn: 'blur'}" required>
                                <span class="messages" ng-show="userForm.nombre.$error">
                                    <span ng-show="userForm.nombre.$error.required">El campo es obligatorio.</span>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label>Email Estudiante</label>
                                <input id="email_estudiante" name="email_estudiante" class="form-control" type="email" value="{{cn.id.email_estudiante}}" ng-model-options="{updateOn: 'blur'}">
                                <span class="messages" ng-show="userForm.email_acudiente.$error">
                                    <span ng-show="userForm.email_estudiante.$error.email">Formato de email incorrecto.</span>
                                </span>
                            </div>
                            <div class="col-6">
                                <label>Telefono Estudiante</label>
                                <input id="telefono_estudiante" name="telefono_estudiante" class="form-control" type="number" value="{{cn.id.telefono_estudiante}}" ng-model-options="{updateOn: 'blur'}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label>Nombre Acudiente</label>
                                <input id="nombre_acudiente" name="nombre_acudiente" class="form-control" type="text" value="{{cn.id.nombre_acudiente}}" ng-model-options="{updateOn: 'blur'}" required>
                                <span class="messages" ng-show="userForm.nombre_acudiente.$error">
                                    <span ng-show="userForm.nombre_acudiente.$error.required">El campo es obligatorio.</span>
                                </span>
                            </div>
                            <div class="col-6">
                                <label>Telefono Acudiente</label>
                                <input id="telefono_acudiente" name="telefono_acudiente" class="form-control" type="number" value="{{cn.id.telefono_acudiente}}" ng-model-options="{updateOn: 'blur'}">
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label>Email Acudiente</label>
                                <input id="email_acudiente" name="email_acudiente" class="form-control" type="text" value="{{cn.id.email_acudiente}}" ng-model-options="{updateOn: 'blur'}" required>
                                <span class="messages" ng-show="userForm.email_acudiente.$error">
                                    <span ng-show="userForm.email_acudiente.$error.required">El campo es obligatorio.</span>
                                    <span ng-show="userForm.curso.$error.email">Formato de email incorrecto.</span>
                                </span>
                            </div>
                            <div class="col-6" >
                                <label>Curso</label>
                                <select id="curso" class="form-control">
                                </select>
                                <span class="messages" ng-show="userForm.curso.$error">
                                    <span ng-show="userForm.curso.$error.required">El campo es obligatorio.</span>
                                </span>
                            </div>
                        </div>
                <div><br></div>
                <div class="row">
                    <div class="col-3">
                        <button  class="btn btn-primary" ng-click="cn.actualizarEstudiante()">Actualizar Estudiante</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
