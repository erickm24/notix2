<%-- 
    Document   : crear
    Created on : 20/09/2021, 4:32:45 p. m.
    Author     : siste
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src = "http://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script> 
        <script src = "docentesController.js"></script>
        <title>Nuevo Docente</title>
    </head>
    <body>
        
                <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <div>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a class="navbar-brand" href="/notix/index.jsp">
                            <img src="/notix/img/notixlogo.png" alt="" width="30" height="24" class="d-inline-block align-text-top">
                            Notix
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/notix/cursos/index.jsp">Cursos</a>
                    </li>     
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/asignaturas/index.jsp">Asignaturas</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/notix/estudiantes/index.jsp">Estudiantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/notix/docentes/index.jsp">Docentes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notix/notas/index.jsp">Notas</a>
                    </li>
                    <span class="navbar-text">
                        Bienvenidos al Sistema Notix     
                    </span>
                </ul>
            </div>
        </nav>
        
        <div class="container-fluid" ng-app = "notix" ng-controller = "docentesController as cn">
            <form name="userForm" novalidate>
                <div class="row">
                    <div class="col-12">
                        <center><h1>Nuevo Docente</h1></center> 
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <label>Cedula</label>
                                <input name="cedula" class="form-control" type="text" ng-model="cn.cedula" ng-model-options="{updateOn: 'blur'}" required>
                                <span class="messages" ng-show="userForm.cedula.$error">
                                    <span ng-show="userForm.cedula.$error.required">El campo es obligatorio.</span>
                                </span>
                            </div>
                             <div class="col-6">
                                <label>Nombre</label>
                                <input name="nombre" class="form-control" type="text" ng-model="cn.nombre" ng-model-options="{updateOn: 'blur'}" required>
                                <span class="messages" ng-show="userForm.nombre.$error">
                                    <span ng-show="userForm.nombre.$error.required">El campo es obligatorio.</span>
                                </span>
                            </div>
                            <div class="col-6">
                                <label>Telefono</label>
                                <input name="telefono" class="form-control" type="text" ng-model="cn.telefono" ng-model-options="{updateOn: 'blur'}" required>
                            </div>
                            <div class="col-6">
                                <label>Email</label>
                                <input name="email" class="form-control" type="text" ng-model="cn.email" ng-model-options="{updateOn: 'blur'}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div><br></div>
                <div class="row">
                    <div class="col-3">
                        <button  class="btn btn-primary" ng-click="cn.guardarDocente()">Guardar Docente</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
