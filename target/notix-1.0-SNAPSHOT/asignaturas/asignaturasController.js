angular.module('notix', [])
                .controller('asignaturasController', ['$scope', function ($scope) {
                        $scope.user = {};

                        $scope.update = function () {
                            console.log($scope.user);
                        };

                        $scope.reset = function (form) {
                            $scope.user = {};
                            if (form) {
                                form.$setPristine();
                                form.$setUntouched();
                            }
                        };

                        $scope.reset();
                    }]);
        var app = angular.module('notix', []);
        app.controller('asignaturasController', ['$http', controladorAsignaturas]);
        //
        function validar() {
            return true;
        }
        function controladorAsignaturas($http) {
            var cn = this;
            var id = "";

            cn.getGET = function ()
            {
                cn.id = 0;
                // capturamos la url
                var loc = document.location.href;
                // si existe el interrogante
                if(loc.indexOf('?')>0)
                {
                    // cogemos la parte de la url que hay despues del interrogante
                    var getString = loc.split('?')[1];
                    // obtenemos un array con cada clave=valor
                    var GET = getString.split('&');
                    var get = {};
                    // recorremos todo el array de valores
                    for(var i = 0, l = GET.length; i < l; i++){
                        var tmp = GET[i].split('=');
                        get[tmp[0]] = unescape(decodeURI(tmp[1]));
                    }
                    cn.id = get;
                    //alert(" id: "+cn.id.id);
                    
                    var url = "PeticionesAsignaturas.jsp";
                var params = {
                    proceso: "listardocentes"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: params
                }).then(function (res) {
                    cn.docentes = res.data.Docentes;
                    var combodocente = document.getElementById("docente");
                    var option;
                    for(docente of res.data.Docentes){
                        option = document.createElement('option');
                        option.text = docente.nombre;

                    if(option.text == get.docente){
                            option.selected = true;
                        }
                        option.value = docente.cedula;
                        combodocente.add(option);
                    }
                    
                });
           //-----------------agregado listar cursos -----------------------
                    
                var url = "PeticionesAsignaturas.jsp";
                var params = {
                    proceso: "listarcursos"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: params
                }).then(function (res) {
                    cn.cursos = res.data.Cursos;
                    var combocurso = document.getElementById("curso");
                    var option;
                    for(curso of res.data.Cursos){
                        option = document.createElement('option');
                        option.text = curso.nombre;
                        //alert("option: "+option.text+" get: "+cn.id.curso);
                        if(option.text == get.curso){
                            option.selected = true;
                        }
                        option.value = curso.id;
                        combocurso.add(option);
                    }
                    
                });


                    return get;
                }
            };

            cn.abrirEditar = function ($id,$nombre,$docente,$curso) {
                window.location.href="editar.jsp?id="+$id+" &nombre="+$nombre+" &docente="+$docente+" &curso="+$curso;
            };
            
            cn.abrirNuevo = function ($id,$nombre,$docente,$curso) {
                window.location.href="crear.jsp?id="+$id+" &nombre="+$nombre+" &docente="+$docente+" &curso="+$curso;
            };
            
            cn.abrirLista = function () {
                window.location.href="index.jsp";
            };

            cn.listarAsignaturas = function () {
                var url = "PeticionesAsignaturas.jsp";
                var params = {
                    proceso: "listarasignatura"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: params
                }).then(function (res) {
                    cn.asignaturas = res.data.Asignaturas;
                });
            };
            
                cn.listarCursos = function () {
                var url = "PeticionesAsignaturas.jsp";
                var params = {
                    proceso: "listarcursos"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: params
                }).then(function (res) {
                    cn.cursos = res.data.Cursos;
                    var combocurso = document.getElementById("curso");
                    for(curso of res.data.Cursos){
                        var option = document.createElement('option');
                        option.text = curso.nombre;
                        option.value = curso.id;
                        combocurso.add(option);
                        
                    }
                });
                
                return cn.cursos;
            };
            
            
            
            //----------------------Listar Docentes-----------------
            
            cn.listarDocentes = function () {
                var url = "PeticionesAsignaturas.jsp";
                var params = {
                    proceso: "listardocentes"
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: params
                }).then(function (res) {
                    cn.docentes = res.data.Docentes;
                    var combodocente = document.getElementById("docente");
                    
                    for(docente of res.data.Docentes){
                        var option = document.createElement('option');
                        option.text = docente.nombre;
                        option.value = docente.cedula;
                                              
                        combodocente.add(option);
                        
                    }
                });
                
                return cn.docentes;
            };
            
            //----------------------------------------------------------
            
            cn.guardarAsignatura = function () {
                var asignatura = {
                    proceso: "guardarasignatura",            
                    nombre: document.getElementById("nombre").value,
                    docente: document.getElementById("docente").value,
                    curso: document.getElementById("curso").value
                };
                
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: asignatura
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[asignatura.proceso] === true) {
                            alert("Guardado con éxito");
                            cn.abrirLista();
                            //                                                            cn.listarContactos();
                        } else {
                            alert("Por favor verifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };

            cn.eliminarAsignatura = function ($id) {
                var asignatura = {
                    proceso: "eliminarasignatura",
                    id: $id
                };
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: asignatura
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[asignatura.proceso] === true) {
                            alert("Eliminado con éxito");
                            cn.listarAsignaturas();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };
            
            cn.actualizarAsignatura = function () {

                var asignatura = {
                    proceso: "actualizarasignatura",
                    id: document.getElementById("id").value,
                    nombre: document.getElementById("nombre").value,
                    docente: document.getElementById("docente").value,
                    curso: document.getElementById("curso").value
                    
                    
                };
                
                $http({
                    method: 'POST',
                    url: 'PeticionesAsignaturas.jsp',
                    params: asignatura
                }).then(function (res) {
                    if (res.data.ok === true) {
                        if (res.data[asignatura.proceso] === true) {
                            alert("Actualizar asignatura con éxito");
                            cn.abrirLista();
                        } else {
                            alert("Por favor vefifique sus datos");
                        }
                    } else {
                        alert(res.data.errorMsg);
                    }
                });

            };

        }