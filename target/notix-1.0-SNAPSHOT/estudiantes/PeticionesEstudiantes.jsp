<%-- 
    Document   : Archivo de peticiones
    Created on : dd/mm/yyyy, hh:mm: AM/PM
    Author     : nombre autor

--%>


<%@page import="java.lang.System"%>
<%@page import="modelo.Estudiantes"%>
<%@page import="modelo.Cursos"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="application/json;charset=iso-8859-1" language="java" pageEncoding="iso-8859-1" session="true"%>

<%    // Iniciando respuesta JSON.
    String respuesta = "{";

    //Lista de procesos o tareas a realizar 
    List<String> tareas = Arrays.asList(new String[]{
        "actualizarestudiante",
        "eliminarestudiante",
        "listarestudiantes",
        "guardarestudiante",
        "listarcursos"
    });
    
    String proceso = "" + request.getParameter("proceso");
    Estudiantes c = new Estudiantes();
    Cursos cu = new Cursos();
    // Validaci�n de par�metros utilizados en todos los procesos.
    if (tareas.contains(proceso)) {
        respuesta += "\"ok\": true,";
        // ------------------------------------------------------------------------------------- //
        // -----------------------------------INICIO PROCESOS----------------------------------- //
        // ------------------------------------------------------------------------------------- //
        if (proceso.equals("guardarestudiante")) {
            String id = request.getParameter("id");
            String nombre = request.getParameter("nombre");
            String emailestudiante = request.getParameter("emailestudiante");
            String telefonoestudiante = request.getParameter("telefonoestudiante");
            String nombreacudiente = request.getParameter("nombreacudiente");
            String emailacudiente = request.getParameter("emailacudiente");   
            String telefonoacudiente = request.getParameter("telefonoacudiente");
            String curso = request.getParameter("curso");

            c.crearEstudiante(id, nombre, emailestudiante, telefonoestudiante, nombreacudiente, emailacudiente, telefonoacudiente, curso);
            
            System.out.println(c.getNombre());
            
            if (c.guardarEstudiante()) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }

        } else if (proceso.equals("eliminarestudiante")) {
            String id = request.getParameter("id");
            //su codigo ac�
            if (c.borrarEstudiante(id)) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }

        } else if (proceso.equals("listarcursos")) {
            //su codigo ac�
            try {
                List<Cursos> lista = cu.getListaCursos();
                respuesta += "\"" + proceso + "\": true,\"Cursos\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Cursos\":[]";
                Logger.getLogger(Cursos.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("listarestudiantes")) {
            //su codigo ac�
            try {
                List<Estudiantes> lista = c.getListaEstudiantes();
                respuesta += "\"" + proceso + "\": true,\"Estudiantes\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Estudiantes\":[]";
                Logger.getLogger(Estudiantes.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("actualizarestudiante")) {
            String id = request.getParameter("id");
            String nombre = request.getParameter("nombre");
            String emaile = request.getParameter("emaile");
            String telefonoe = request.getParameter("telefonoe");
            String nombrea = request.getParameter("nombrea");
            String telefonoa = request.getParameter("telefonoa");
            String emaila = request.getParameter("emaila");
            String curso = request.getParameter("curso");
            c.crearEstudiante(id, nombre, emaile, telefonoe, nombrea, emaila, telefonoa, curso);
            if (c.actualizarEstudiante()) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }
        }

        // ------------------------------------------------------------------------------------- //
        // -----------------------------------FIN PROCESOS-------------------------------------- //
        // ------------------------------------------------------------------------------------- //
        // Proceso desconocido.
    } else {
        respuesta += "\"ok\": false,";
        respuesta += "\"error\": \"INVALID\",";
        respuesta += "\"errorMsg\": \"Lo sentimos, los datos que ha enviado,"
                + " son inv�lidos. Corrijalos y vuelva a intentar por favor.\"";
    }
    // Usuario sin sesi�n.
    // Responder como objeto JSON codificaci�n ISO 8859-1.
    respuesta += "}";
    response.setContentType("application/json;charset=iso-8859-1");
    out.print(respuesta);
%>
